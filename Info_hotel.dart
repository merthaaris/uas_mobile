import 'package:flutter/material.dart';

class CarItem {
  final String name;
  final double price;
  final String bed;
  final String pool;
  final String location;
  final String path;

  CarItem(
      {@required this.name,
      @required this.price,
      @required this.bed,
      @required this.pool,
      @required this.location,
      @required this.path});
}

HotelList allCars = HotelList(cars: [
  CarItem(
      name: 'Griya Hotel',
      price: 83,
      pool: '2',
      bed: '2',
      location: 'Amed',
      path: 'images/assets/car1.jpeg'),
  CarItem(
      name: 'Amazon Hotel',
      price: 207,
      pool: '4',
      bed: '4',
      location: 'Canggu',
      path: 'images/assets/car2.jpeg'),
  CarItem(
      name: 'Garuda Hotel',
      price: 90,
      pool: '2',
      bed: '2',
      location: 'Ubud',
      path: 'images/assets/car3.jpeg'),
  CarItem(
      name: 'Hotel Arjuna',
      price: 101,
      pool: '3',
      bed: '3',
      location: 'Kuta',
      path: 'images/assets/car4.jpeg'),
  CarItem(
      name: 'Hotel Balangan ',
      price: 195,
      pool: '4',
      bed: '5',
      location: 'Jimbaran',
      path: 'images/assets/car5.jpeg'),
  CarItem(
      name: 'Kuta Hotel',
      price: 250,
      pool: '8',
      bed: '8',
      location: 'Kuta',
      path: 'images/assets/car6.jpeg'),
]);

class HotelList {
  List<CarItem> cars;

  HotelList({this.cars});
}
