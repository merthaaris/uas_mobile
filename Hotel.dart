import 'package:flutter/material.dart';
import 'package:indocars/Detail_hotel.dart';
import 'Detail_hotel.dart';
import 'Info_hotel.dart';

class HotelTs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      physics: ScrollPhysics(),
      shrinkWrap: true,
      itemCount: allCars.cars.length,
      itemBuilder: (ctx, i) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (ctx) => DetailHotel(
                  name: allCars.cars[i].name,
                  location: allCars.cars[i].location,
                  pool: allCars.cars[i].pool,
                  price: allCars.cars[i].price,
                  path: allCars.cars[i].path,
                  bed: allCars.cars[i].bed,
                ),
              ),
            );
          },
          child: Container(
              margin: EdgeInsets.only(
                  top: i.isEven ? 0 : 10, bottom: i.isEven ? 10 : 0),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black26, blurRadius: 5, spreadRadius: 1)
                  ]),
              child: Column(
                children: [
                  Hero(
                      tag: allCars.cars[i].name,
                      child: Image.asset(allCars.cars[i].path)),
                  Text(
                    allCars.cars[i].name,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                  Text(
                    (allCars.cars[i].price).toString(),
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  Text('per week')
                ],
              )),
        ),
      ),
      gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
    );
  }
}
