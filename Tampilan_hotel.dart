import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:indocars/Hotel.dart';

class Tampilanhotel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text("Travel Hotel Bali"),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
          ),
          Text(
            'Available Hotel',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: HotelTs(),
          ),
        ],
      ),
    );
  }
}
