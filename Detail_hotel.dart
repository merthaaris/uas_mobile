import 'package:flutter/material.dart';
import 'Spesifik_hotel.dart';

void _showSimpleDialog(context) {
  showDialog(
    context: context,
    builder: (context) {
      return SimpleDialog(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 10.0, right: 10.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    "Permintaan Terkirim",
                    style: TextStyle(fontSize: 20.0),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )
              ],
            ),
          ),
        ],
      );
    },
  );
}

class DetailHotel extends StatelessWidget {
  final String name;
  final double price;
  final String bed;
  final String pool;
  final String location;
  final String path;

  DetailHotel(
      {this.name, this.price, this.bed, this.pool, this.location, this.path});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text("Travel Hotel Bali"),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Text(
            name,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 30,
            ),
          ),
          Text(
            location,
            style: TextStyle(
              fontSize: 15,
            ),
          ),
          Hero(tag: name, child: Image.asset(path)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SpesifikHotel(
                name: '1 week',
                price: price * 7,
                name2: 'USD',
              ),
              SpesifikHotel(
                name: '4 day',
                price: price * 4,
                name2: 'USD',
              ),
              SpesifikHotel(
                name: '1 day',
                price: price * 1,
                name2: 'USD',
              )
            ],
          ),
          SizedBox(height: 20),
          Text(
            'KETERANGAN',
            style: TextStyle(
                color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SpesifikHotel(
                name: 'Bed',
                name2: bed,
              ),
              SpesifikHotel(
                name: 'Pool',
                name2: pool,
              )
            ],
          ),
          SizedBox(height: 10),
          RaisedButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            onPressed: () {
              _showSimpleDialog(context);
            },
            padding: EdgeInsets.all(10.0),
            color: Colors.blueAccent,
            child: Text(
              'Sewa',
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
